//
//  FirstViewController.swift
//  UsodeControles
//
//  Created by carlos pumayalla on 9/29/21.
//  Copyright © 2021 empresa. All rights reserved.
//

import UIKit

class FirstViewController:UIViewController {
    
    var nr1:Double = 0
    var nr2:Double = 0
    var rpta:Double = 0
    
    @IBOutlet weak var txtNumero1: UITextField!
    @IBOutlet weak var TxtNumero2: UITextField!
    @IBOutlet weak var txtRespuesta: UITextField!
    @IBAction func btnSumar(_ sender: Any) {
        
        if Double(txtNumero1.text!) != nil && Double(TxtNumero2.text!) != nil{
            nr1 = Double(txtNumero1.text!)!
            nr2 = Double(TxtNumero2.text!)!
            rpta = nr1 + nr2
            txtRespuesta.text = String(rpta)
        }else{
            mostrarAlerta(titulo: "Error", mensaje: "Debe introducir valores numéricos")
            print("Error al realizar la operación")
        }
    }
    @IBAction func btnRestar(_ sender: Any) {
        if Double(txtNumero1.text!) != nil && Double(TxtNumero2.text!) != nil{
            nr1 = Double(txtNumero1.text!)!
            nr2 = Double(TxtNumero2.text!)!
            rpta = nr1 - nr2
            txtRespuesta.text = String(rpta)
        }else{
            mostrarAlerta(titulo: "Error", mensaje: "Debe introducir valores numéricos")
            print("Error al realizar la operación")
        }
    }
    @IBAction func btnDiv(_ sender: Any) {
        if Double(txtNumero1.text!) != nil && Double(TxtNumero2.text!) != nil{
            nr1 = Double(txtNumero1.text!)!
            nr2 = Double(TxtNumero2.text!)!
            rpta = nr1 / nr2
            txtRespuesta.text = String(rpta)
        }else{
            mostrarAlerta(titulo: "Error", mensaje: "Debe introducir valores numéricos")
            print("Error al realizar la operación")
        }
    }
    @IBAction func btnMult(_ sender: Any) {
        if Double(txtNumero1.text!) != nil && Double(TxtNumero2.text!) != nil{
            nr1 = Double(txtNumero1.text!)!
            nr2 = Double(TxtNumero2.text!)!
            rpta = nr1 * nr2
            txtRespuesta.text = String(rpta)
        }else{
            mostrarAlerta(titulo: "Error", mensaje: "Debe introducir valores numéricos")
            print("Error al realizar la operación")
        }
    }
    @IBAction func btnLimpiar(_ sender: Any) {
        Limpiar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ocultarTeclado))
        view.addGestureRecognizer(tap)
    }

    func Limpiar(){
        self.txtNumero1.text = "0"
        self.TxtNumero2.text = "0"
        self.txtRespuesta.text = "0"
    }
    
    @objc func ocultarTeclado() {
        view.endEditing(true)
    }
    
    func mostrarAlerta (titulo: String, mensaje: String){
        let alerta = UIAlertController(title: titulo, message: mensaje, preferredStyle: .alert)
        let limpiar = UIAlertAction(title: "Limpiar", style: .default, handler: {(action) in
            self.Limpiar()
        })
        let cancelar = UIAlertAction(title: "Cancelar", style: .cancel, handler: {(action) in
        })
        alerta.addAction(limpiar)
        alerta.addAction(cancelar)
        present(alerta, animated: true, completion: nil)
        
    }

}

